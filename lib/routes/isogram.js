const isogramCtrl = require('../controllers/isogram')

const internals = {}

internals.routes = [
  { method: 'POST', path: '/isogram-check', options: isogramCtrl.isIsogram},
]

module.exports = internals.routes