'use strict'
const Hapi = require('@hapi/hapi')
const Inert = require('@hapi/inert')
const Vision = require('@hapi/vision')
const HapiSwagger = require('hapi-swagger')
const Joi = require('@hapi/joi')

const route = require('./routes')
const Database = require('./common/database');

(async () => {
  const server = Hapi.server({
    port: 3000,
    host: 'localhost'
  })

  const swaggerOptions = {
    info: {
      title: 'API Pangram Documentation',
      version: '0.0.1'
    }
  }

  await server.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
  ])

  try {
    await server.start()
    console.log('Server running at:', server.info.uri)
  } catch (err) {
    console.log(err)
  }

  const ex = [{
    method: 'POST',
    path: '/{name}',
    options: {
      handler: (request, h) => {
        reply(`Hello, ${request.params.name}!`);
      },
      validate: {
          payload: Joi.object({
              post: Joi.string().min(1).max(140),
              date: Joi.date().required()
          })
        },
        description: 'hello world',
        notes: 'Returns hello world',
        tags: ['api','pangram'],
      }  
    
  }];
  //server.route(ex);
  server.route(route);

  console.log('Server running on %s', server.info.uri)
})()

process.on('unhandledRejection', (err) => {
  console.log(err)
  process.exit(1)
})
