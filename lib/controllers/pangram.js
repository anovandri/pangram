const Boom = require('@hapi/boom')
const Joi = require('@hapi/joi')

const Pangram = require('../model/pangram')

const pangram = new Pangram()

const internals = {}

internals.post = {
  handler: (request, reply) => {
    try {
      const sourcePayload = request.payload
      const sourceJsonPayload = JSON.parse(JSON.stringify(sourcePayload))
      pangram.post(sourceJsonPayload).then((data) => {
        if (data.length > 0) {
          pangram.createPangram(data).then((result) => {
            pangram.savePangram(sourceJsonPayload.source, result)
          })
        }
      })
      const response = { status: 'In Progress', code: 0 }
      return response;
    } catch (err) {
      return Boom.badRequest(err)
    }
  },
  validate: {
    payload: Joi.object({
        source: Joi.string(),
    })
  },
  description: 'Generate pangram from url site picked',
  notes: 'Pass json as a request body; example : {"source": "http://www.gutenberg.org/files/58663/58663-0.txt"}',
  tags: ['api', 'pangram'],
}

internals.getRandom = {
  handler: async (request, reply) => {
    const result = await pangram.getRandomPangram()
    return result
  },
  description: 'Get random pangram from server',
  notes: 'Get random pangram from server',
  tags: ['api', 'pangram'],
}

module.exports = internals
