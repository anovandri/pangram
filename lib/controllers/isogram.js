const Isogram = require('../model/isogram')

const internals = {};

internals.isIsogram = {
  handler: async (request, reply) => {
    try {
      const sourcePayload = request.payload;
      const sourceJsonPayload = JSON.parse(JSON.stringify(sourcePayload));
      const isValid = await Isogram.isIsogram(sourceJsonPayload.word)
      return isValid;
    } catch (err) {
      return Boom.badRequest(err);
    }
  },
  validate: {
    payload: Joi.object({
      word: Joi.string(),
    })
  },
  description: 'Check whether word is isogram or not ',
  notes: 'Pass json as a request body; example : {"word": "halo"}',
  tags: ['api', 'pangram'],
};

module.exports = internals

