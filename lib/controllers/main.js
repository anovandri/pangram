const internals = {}

internals.home = {
  handler: (request, h) => {
    return 'hello world';
  },
  description: 'hello world',
  notes: 'Returns hello world',
  tags: ['api','pangram'],
}

module.exports = internals
