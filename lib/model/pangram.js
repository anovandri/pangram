const axios = require('axios')
const httpHeader = require('axios/lib/adapters/http')
const guttenbergFile = require('../model/guttenberg_file')
const guttenbergPangram = require('../model/guttenberg_pangram')
const ObjPangram = require('./objpangram')

const internals = {}

class Pangram {
  constructor () {
    let { instance } = internals
    if (!instance) {
      instance = this
    }
    return instance
  }

  post (payload) {
    const promise = new Promise((resolve, reject) => {
      axios.get(payload.source, {
        responseType: 'stream',
        adapter: httpHeader
      }).then((response) => {
        const stream = response.data
        stream.on('data', (chunk) => {
          const buffer = Buffer.from(chunk)
          guttenbergFile.addLine(buffer.toString('utf8'))
        })
        stream.on('end', () => {
          const sentences = []
          const promiseJoinText = guttenbergFile.joinContentText()
          promiseJoinText.then((joinText) => {
            const promiseSentences = guttenbergFile.addSentencesArray(joinText)
            promiseSentences.then((sentences) => {
              resolve(sentences)
            })
          })
        })
      })
    })
    return promise
  }

  createPangram (sentences) {
    const promise = new Promise((resolve, reject) => {
      guttenbergPangram.sentences = sentences
      guttenbergPangram.run().then((result) => {
        resolve(result)
      })
    })
    return promise
  }

  savePangram (source, results) {
    const promise = new Promise((resolve, reject) => {
      results.forEach(result => {
        const objpangram = new ObjPangram({
          fileName: source,
          pangram: result
        })
        objpangram.save()
          .then(doc => {
            console.log(doc)
          })
          .catch(err => {
            console.error(err)
          })
      })
    })
    return promise
  }

  getRandomPangram () {
    const promise = new Promise((resolve, reject) => {
      ObjPangram.count().exec(function (err, count) {
        const random = Math.floor(Math.random() * count)
        ObjPangram.findOne().skip(random).exec(function (err, result) {
          resolve(result)
        })
      })
    })
    return promise
  }
}

module.exports = Pangram
