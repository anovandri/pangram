const Path = require("path");
const util = require("util");
const exec = util.promisify(require("child_process").exec);

const internals = {};

const errors = {
  notValidEnglishWordCode: 400,
  notValidEnglishWordDesc: "Your word is not valid",
  notValidIsogramCode: 401,
  notValidIsogramDesc: "Your word is not isogram"
};

const dictionaryFileName = Path.resolve(
  __dirname,
  "../../static/dictionary.txt"
);
class Isogram {
  constructor() {
    let { instance } = internals;
    if (!instance) {
      instance = this;
    }
    return instance;
  }

  async isValidEnglishWord(word) {
    const englishWordsPromise = await new Promise(async (resolve, reject) => {
      exec(
        "egrep '" + word.toUpperCase() + "' " + dictionaryFileName,
        (error, stdout, stderr) => {
          if (error) {
            reject({
              code: errors.notValidEnglishWordCode,
              desc: errors.notValidEnglishWordDesc
            });
          } else {
            resolve(stdout);
          }
        }
      );
    });

    const validWords = englishWordsPromise.split("\n").filter(item => {
      return item === word.toUpperCase();
    });
    return validWords;
  }

  async isIsogram(word) {
    const result = await this.isValidEnglishWord(word);
    if (result.length > 0) {
      const characters = result[0]
        .replace(/\W/g, '')
        .toLocaleLowerCase()
        .split('');
      const output = characters.filter((item, index) => {
        return characters.indexOf(item) === index;
      });
      
      if (output.length === word.length) {
          return Promise.resolve(true);
      } else {
          return Promise.reject({
            code: errors.notValidIsogramCode,
            desc: errors.notValidIsogramDesc
          })
      }
    } 
  }
}

module.exports = Isogram

