const Mongoose = require('mongoose')
const Schema = Mongoose.Schema

const internals = {}

internals.ObjPangramSchema = new Schema({
  fileName: { type: String, trim: true },
  pangram: { type: String },
  date_created: { type: Date, default: Date.now }
})

module.exports = Mongoose.model('ObjPangram', internals.ObjPangramSchema)
