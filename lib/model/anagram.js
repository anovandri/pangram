const Path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const anagram = (function() {
    const dictionaryFileName = Path.resolve(__dirname, '../../static/dictionary.txt');
    const errors = {
        notValidFormatCode: 300,
        notValidFormatDesc: 'Your request is invalid',
        notValidEnglishWordCode: 301,
        notValidEnglishWordDesc: 'Your word is not valid',
    }
    const tempOut = [];
    const wordCount = 2;

    async function isValidAnagram(words) {

    };

    async function functionEgrep(words) {
        return new Promise((resolve, reject) => {
            words.forEach((word) => {
                exec("egrep '" + word.toUpperCase() + "' " + dictionaryFileName, (error, stdout, stderr)  => {
                    if (error) {
                        reject({
                            code: errors.notValidEnglishWordCode,
                            desc: errors.notValidEnglishWordDesc
                        })
                    } else {
                        resolve(stdout);
                    }
                })
             });
        });
    }

    async function isValidEnglishWord(words) {
        return await functionEgrep(words);
    };
    
    async function isValidWords(words) {
        if (!Array.isArray(words)) {
            return Promise.reject({
                code: errors.notValidFormatCode,
                desc: errors.notValidFormatDesc
            })
        }

        if (words.length != wordCount) {
            return Promise.reject({
                code: errors.notValidFormatCode,
                desc: errors.notValidFormatDesc
            })
        }
        return {
            words: words,
            isValidEnglishWord: isValidEnglishWord(words)
        }
    }

    return {
        process: async function(words) {
            return (await isValidWords(words)).isValidEnglishWord;
        }
    }
})();



